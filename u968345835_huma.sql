
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Ven 18 Décembre 2015 à 10:23
-- Version du serveur: 10.0.22-MariaDB
-- Version de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `u968345835_huma`
--

-- --------------------------------------------------------

--
-- Structure de la table `Camp`
--

CREATE TABLE IF NOT EXISTS `Camp` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nomLocalisation` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `gerant` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `Camp`
--

INSERT INTO `Camp` (`ID`, `nomLocalisation`, `tel`, `gerant`) VALUES
(2, 'Bahamas', '0245365421', 2),
(1, 'Irak', '0325489710', 6),
(3, 'Mongolie', '0385974162', 3),
(4, 'Grece', '0152984123', 4),
(5, 'Antarctique', '0574652031', 5);

-- --------------------------------------------------------

--
-- Structure de la table `Categorie`
--

CREATE TABLE IF NOT EXISTS `Categorie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `nom` (`nom`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `Categorie`
--

INSERT INTO `Categorie` (`ID`, `nom`) VALUES
(1, 'Outils'),
(2, 'Medicaments'),
(3, 'Nourriture'),
(4, 'Materiel Technologique'),
(5, 'Materiel de campement');

-- --------------------------------------------------------

--
-- Structure de la table `Logisticien`
--

CREATE TABLE IF NOT EXISTS `Logisticien` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `datenais` date NOT NULL,
  `url_photo` int(11) DEFAULT NULL,
  `camp` int(11) NOT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mdp` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `Logisticien`
--

INSERT INTO `Logisticien` (`ID`, `nom`, `prenom`, `datenais`, `url_photo`, `camp`, `login`, `mdp`) VALUES
(1, 'Gillet', 'Martin', '1992-01-02', NULL, 1, 'GilletM', 'ab4f63f9ac65152575886860dde480a1'),
(2, 'Demaegdt', 'Theophile', '1985-10-03', NULL, 1, 'theophile', 'e1f82dfe0b7f23af859006f362545a85'),
(3, 'Ballin', 'Marie', '1996-12-06', NULL, 3, 'ballinm', '924714634faba95ae75d9439b20b8571'),
(4, 'Freitas', 'Thomas', '1996-03-27', NULL, 4, 'freitast', 'ef6e65efc188e7dffd7335b646a85a21'),
(5, 'Degardin', 'Louis', '1993-08-01', NULL, 5, 'louis', '9684dd2a6489bf2be2fbdd799a8028e3'),
(6, 'Brisach', 'Amaury', '1995-11-22', NULL, 1, 'amaury', '3261026849ff5d030ea1cd79f05db214');

-- --------------------------------------------------------

--
-- Structure de la table `Objet`
--

CREATE TABLE IF NOT EXISTS `Objet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Unknow',
  `ID_cat` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Contenu de la table `Objet`
--

INSERT INTO `Objet` (`ID`, `nom`, `ID_cat`) VALUES
(32, 'telephone', 4),
(31, 'Nutella', 3),
(30, 'Chocapic', 3),
(29, 'Boite de ravioli', 3),
(28, 'Chaise', 5),
(27, 'Table', 5),
(26, 'Marteau', 1),
(25, 'Ordinateur', 4),
(24, 'Jerican', 5),
(23, 'Tente', 5);

-- --------------------------------------------------------

--
-- Structure de la table `Refugies`
--

CREATE TABLE IF NOT EXISTS `Refugies` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datenais` date DEFAULT NULL,
  `sexe` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `url_photo` text COLLATE utf8_unicode_ci,
  `camp` int(11) NOT NULL,
  `dateEntree` date NOT NULL,
  `poids` int(11) DEFAULT NULL,
  `taille` int(11) DEFAULT NULL,
  `cheveux` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `yeux` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `signeDistinctif` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peau` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `Refugies`
--

INSERT INTO `Refugies` (`ID`, `nom`, `prenom`, `datenais`, `sexe`, `url_photo`, `camp`, `dateEntree`, `poids`, `taille`, `cheveux`, `yeux`, `signeDistinctif`, `peau`) VALUES
(1, 'Convenant', 'Jean-Claude', '2015-12-01', 'H', '', 2, '2015-12-01', 75, 175, 'brun', 'marron', 'aucun', 'europeen'),
(2, 'Clanerd', 'Roland', '1986-02-28', 'H', '', 3, '1999-03-15', 72, 178, 'noir', 'bleu', 'piercing', 'europeen'),
(3, 'Lucas', 'Pino', '1990-11-04', 'H', '', 1, '2013-01-01', 90, 200, 'noir', 'marron', 'aucun', 'africain'),
(4, 'Yang', 'Clodia', '1990-10-24', 'F', '', 3, '2002-08-09', 50, 164, 'autre', 'autre', 'autre', 'asiatique'),
(5, 'Martin', 'Jeanne', '1980-05-25', 'F', '', 4, '2002-02-02', 68, 174, 'noir', 'vert', 'aucun', 'europeen'),
(6, 'dufranc', 'Pierre', '1987-01-04', 'M', '', 2, '1999-06-08', 90, 189, 'blond', 'marron', 'cicatrice', 'indien');

-- --------------------------------------------------------

--
-- Structure de la table `Stock`
--

CREATE TABLE IF NOT EXISTS `Stock` (
  `IDCamp` int(11) NOT NULL,
  `IDObjet` int(11) NOT NULL,
  `nombre` int(11) NOT NULL,
  PRIMARY KEY (`IDCamp`,`IDObjet`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Stock`
--

INSERT INTO `Stock` (`IDCamp`, `IDObjet`, `nombre`) VALUES
(1, 27, 100),
(5, 26, 0),
(4, 26, 22),
(3, 26, 0),
(2, 26, 0),
(4, 24, 0),
(5, 25, 0),
(5, 24, 0),
(2, 24, 2),
(3, 24, 0),
(4, 23, 0),
(1, 23, 0),
(3, 23, 0),
(1, 24, 8),
(2, 25, 0),
(1, 26, 0),
(4, 25, 0),
(2, 23, 8),
(5, 23, 0),
(3, 25, 20),
(1, 25, 0),
(2, 27, 0),
(3, 27, 0),
(4, 27, 0),
(5, 27, 0),
(1, 28, 0),
(2, 28, 36),
(3, 28, 0),
(4, 28, 0),
(5, 28, 0),
(1, 29, 0),
(2, 29, 27),
(3, 29, 0),
(4, 29, 0),
(5, 29, 64),
(1, 30, 12),
(2, 30, 0),
(3, 30, 0),
(4, 30, 1234),
(5, 30, 0),
(1, 31, 0),
(2, 31, 0),
(3, 31, 14),
(4, 31, 0),
(5, 31, 0),
(1, 32, 0),
(2, 32, 3),
(3, 32, 0),
(4, 32, 0),
(5, 32, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

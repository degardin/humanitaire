<?php include('entete.php'); ?>

<html>


<body>

<section id="title"> 
Recherche
</section>

<section id="search">
	<form method="post" action="#">
		<table>
			<tr>
					<div id="cadre">
						<input type="text" name="nom" placeholder="Nom" onfocus="if (this.placeholder == 'Nom') this.placeholder='';" onblur="if (this.placeholder == '') this.placeholder='Nom';" /><br>
						<input type="text" name="taille" placeholder="Taille" onfocus="if (this.placeholder == 'Taille') this.placeholder='';" onblur="if (this.placeholder == '') this.placeholder='Taille';"/><br>
						<input type="text" name="datenais" placeholder="Date de naissance" onfocus="if (this.placeholder == 'Date de naissance') this.placeholder='';" onblur="if (this.placeholder == '') this.placeholder='Date de naissance';"/>
					</div>

					<div id="cadre">
						<input type="text" name="prenom" placeholder="Prenom" onfocus="if (this.placeholder == 'Prenom') this.placeholder='';" onblur="if (this.placeholder == '') this.placeholder='Prenom';"/><br>
						<input type="text" name="poids" placeholder="Poids" onfocus="if (this.placeholder == 'Poids') this.placeholder='';" onblur="if (this.placeholder == '') this.placeholder='Poids';"/>
					</div>

					<div id="cadre">
						<br>
						Sexe : 

						<input id="rad1" name="sexe" type="radio" value="H" /> 
						<label for="rad1">H</label> 

						<input id="rad2" name="sexe" type="radio" value="F" /> 
						<label for="rad2">F</label>

					</div>

				</tr>
				<tr>
					<div id="ld">
						<select id="liste" name="yeux" >
							<option value="">Couleur des Yeux</option>
							<optgroup label="Couleurs :"> 
								<option value="bleu">Bleu</option>
								<option value="vert">Vert</option>
								<option value="marron">Marron</option>
							</optgroup> 
						</select>
						<select id="liste" name="peau" >
							<option value="">Couleur de Peau</option>
							<optgroup label="Couleurs :"> 
								<option value="europeen">Europ�en</option>
								<option value="asiatique">Asiatique</option>
								<option value="africain">Africain</option>
								<option value="indien">Indien</option>
							</optgroup> 
						</select>
						<br>
						<select id="liste" name="cheveux" >
							<option value="">Couleur des Cheveux</option>
							<optgroup label="Couleurs :"> 
								<option value="noir">Noir</option>
								<option value="brun">Brun</option>
								<option value="blond">Blond</option>
								<option value="roux">Roux</option>
								<option value="autre">Autre</option>
							</optgroup> 
						</select>
						<select id="liste" name="signeDistinctif" >
							<option value="">Signe Distinctif</option>
							<optgroup label="Signes :"> 
								<option value="tatouage">Tatouage</option>
								<option value="cicatrice">Cicatrice(s)</option>
								<option value="piercing">Piercing</option>
							</optgroup> 
						</select>
						<select id="liste" name="camp" >
							<option value="">Tous les camps</option>
							<?php
							$camp = $bdd->query("SELECT ID,nomLocalisation FROM Camp ORDER BY ID");
							while ($donnees = $camp->fetch()){
								echo "<option value=".$donnees['ID'].">".$donnees['nomLocalisation'].'</option>';
							}
							?>
						</select>
					</div>
				</tr>


				<tr>
					<input type="submit" name="recherche" value="Rechercher" class="button"/>
					<input type="reset" value="Annuler" class="button"/>
				</tr>

			</form>
		</section>
		
		<?php
		
		if (isset($_POST['recherche'])) {
			
            //Pr�paration de la requ�te
			$sql = 'SELECT * FROM Refugies,Camp';

			$criteres = array(
				'nom' => 'Nom',
				'prenom' => 'Prenom',
				'datenais' => 'Date de naissance',
				'sexe' => 'Genre',
				'url_photo' => 'Photo',
				'camp' => 'Camp',
				'nomLocalisation' => 'Localisation',
				'dateEntree' => 'Date d\'entr�e',
				'poids' => 'Poids',
				'taille' => 'Taille',
				'cheveux' => 'Couleur des cheveux',
				'yeux' => 'Couleur des yeux',
				'signeDistinctif' => 'Signe distinctif',
				'peau' => 'Couleur de peau'
				);

			$valeurs = array();
			
			foreach ($criteres as $critereKey => $critereValue) { 
				// si le crit�re existe 
				if (!empty($_POST[$critereKey])) {
					$filtres[] = $critereKey . ' = :' . $critereKey;
					$valeurs[$critereKey] = $_POST[$critereKey];
				}
			}
			// si le crit�re existe, on l'ajoute dans la requ�te sql � la suite
			if (!empty($filtres)){
				$sql .= ' WHERE ' . implode(' AND ', $filtres);
				$sql .= ' AND Refugies.camp=Camp.ID';
			}
			else{
				$sql .= ' WHERE Refugies.camp=Camp.ID';
			}
			
            //ex�cution de la requ�te
			$requete = $bdd->prepare($sql);
			$res = $requete->execute($valeurs);
			
            //affichage du r�sultat de la requ�te
            //affichage de la premi�re ligne
			echo '<table border="1"><tr class="title">';
			foreach ($criteres as $critereKey => $critereValue){
				echo '<td>' . $critereValue . '</td>';
			}
			echo '</tr>';
			//affichage du contenu du tableau
			while ($donnees = $requete->fetch()) {
				echo '<tr>';
				foreach ($criteres as $critereKey => $critereValue){
					echo '<td>' . $donnees[$critereKey] . '</td>';
				}
				echo '</tr>';
			}
			echo '</table>';
			
            //fermeture du curseur d'analyse des r�sultats
			$requete->closeCursor();
		}
		?>

		
	</body>

	</html>
	